# Mailu

[Mailu](https://mailu.io/1.7/general.html) is a simple yet full-featured mail server as a set of Docker images.

## Access

It is available at [https://mailu.{{ domain }}/](https://mailu.{{ domain }}/) or [http://mailu.{{ domain }}/](http://mailu.{{ domain }}/)

{% if enable_tor %}
It is also available via Tor at [http://mailu.{{ tor_domain }}/](http://mailu.{{ tor_domain }}/)
{% endif %}
